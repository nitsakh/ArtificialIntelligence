import java.util.ArrayDeque;


public class BreadthFirstSearch {

	private int start;
	private Graph g;
	private int visited[]=new int[10];
	private ArrayDeque<Integer> q=new ArrayDeque<>();
	
	public BreadthFirstSearch(){
		
	}
	
	public BreadthFirstSearch(Graph g,int start){
		this.start=start;
		this.g=g;
	}
	
	public void performBFS(){
		BFS(start);
	}
	
	public void BFS(int n){
		System.out.println("Node Visited: "+n);
		
		visited[n]=1;
		for(int i=0;i<g.n;i++){
			if(g.adjmat[i][n]==1 && visited[i]==0){
				q.add(i);
			}
		}
		if(!q.isEmpty())
			BFS(q.pop());
	}
}
