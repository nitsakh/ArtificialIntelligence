import java.util.*;

public class MainClass {

	public static void main(String args[]){
		Scanner s=new Scanner(System.in);
		Graph g=new Graph(s);
		g.acceptGraph();
		g.dispMat();
		/*System.out.println("\nEnter starting node: ");
		DepthFirstSearch d=new DepthFirstSearch(g,s.nextInt());
		d.performDFS();*/
		System.out.println("\nEnter starting node: ");
		BreadthFirstSearch d=new BreadthFirstSearch(g,s.nextInt());
		d.performBFS();
		s.close();
	}
}
